//TCPServerdecrypt.java
package exp3;

import java.io.*;
import java.net.*;
import java.util.HashMap;
import java.util.Map;
import java.security.PublicKey;
import java.util.Base64;
import javax.crypto.Cipher;
import java.security.spec.X509EncodedKeySpec;
import java.security.KeyFactory;
import java.nio.charset.StandardCharsets;

public class TCPServerdecrypt {
    private static final Map<String, String> userDatabase = new HashMap<>();
    private static void loadUserCredentials(String fileName) {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(fileName))) {
            String line;
            while ((line = fileReader.readLine()) != null) {
                String[] parts = line.split(","); // 假设文件中用户名和密码以逗号分隔
                if (parts.length == 2) {
                    String username = parts[0].trim();
                    String password = parts[1].trim();
                    userDatabase.put(username, password);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
    	loadUserCredentials("C:\\Users\\Administrator\\OneDrive\\桌面\\notebooks\\experiments\\exp3\\exp3\\src\\exp3\\credentials.txt");
        try {
            ServerSocket serverSocket = new ServerSocket(2048);
            System.out.println("Server is listening on port 2048...");

            while (true) {
                Socket clientSocket = serverSocket.accept();
                System.out.println("Client connected: " + clientSocket.getInetAddress().getHostAddress());

                // 创建一个新线程来处理客户端连接
                Thread clientThread = new Thread(() -> handleClient(clientSocket));
                clientThread.start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void handleClient(Socket clientSocket) {
        try {
            // Setup streams for communication
        	BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        	PrintWriter writer = new PrintWriter(new OutputStreamWriter(clientSocket.getOutputStream()), true);

            // User authentication
            String username = reader.readLine();
            String password = reader.readLine();
            System.out.println("Received username: " + username);
            System.out.println("Received password: " + password);

            if (!authenticateUser(username, password)) {
                System.out.println("Authentication failed. Closing connection.");
                clientSocket.close();
                return;
            }
            
            writer.println();
            // Authentication successful, send welcome message to client
            String welcomeMessage = "Welcome, " + username + "! You have successfully logged in.";
            writer.println(welcomeMessage);
            
            // get reply
            String replyMessage = reader.readLine();
            System.out.println(replyMessage);
            
            // 接收客户端发送的公钥
            PublicKey publicKey = receivePublicKeyFromClient(reader);
            //System.out.println("Received Public Key: " + publicKey);
            // Keep the connection open for multiple messages
            while (true) {
                String receivedMessage = reader.readLine();
                if (receivedMessage == null) {
                    System.out.println("Client disconnected: " + clientSocket.getInetAddress().getHostAddress());
                    break;
                }
                System.out.println("Received message from client: " + receivedMessage);

                // You can add server-side logic here to process the received message

                // 解密客户端发送的消息
                try {
                    String decryptedText = decrypt(receivedMessage, publicKey);
                    System.out.println("Decrypted Text: " + decryptedText);

                    // 服务器端处理解密后的消息
                    String response = "Message received: " + decryptedText;
                    writer.println(response);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static boolean authenticateUser(String username, String password) {
        // 遍历整个用户数据库
        for (Map.Entry<String, String> entry : userDatabase.entrySet()) {
            String storedUsername = entry.getKey();
            String storedPassword = entry.getValue();

            // 检查用户名和密码是否匹配
            if (storedUsername.equals(username) && storedPassword.equals(password)) {
                return true; // 认证成功
            }
        }

        return false; // 认证失败
    }
    // 接收客户端发送的公钥
    private static PublicKey receivePublicKeyFromClient(BufferedReader reader) {
        try {
            // 从客户端读取公钥字符串
            String publicKeyString = reader.readLine();

            // 将公钥字符串转换为 PublicKey 对象
            byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyString);
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    // 使用RSA公钥解密字符串
    private static String decrypt(String input, PublicKey publicKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, publicKey);
        byte[] decodedBytes = Base64.getDecoder().decode(input.getBytes(StandardCharsets.UTF_8));
        byte[] decryptedBytes = cipher.doFinal(decodedBytes);
        return new String(decryptedBytes);
    }
}
