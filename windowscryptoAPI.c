#include <tchar.h>
#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>
#include <conio.h>

// Link with the Advapi32.lib file.
#pragma comment (lib, "advapi32")

#define MY_ENCODING_TYPE (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define KEYLENGTH  0x00800000
// These additional #define statements are required.
#define ENCRYPT_ALGORITHM CALG_RC4
#define ENCRYPT_BLOCK_SIZE 8
#define MAX_FILE_SIZE 4000000
#define SIGNATURE_SIZE 500

BYTE* pbKeyBlob; // 用来保存导出的公钥
DWORD dwBlobLen;

void HandleError(char* s)
{
    fprintf(stderr, "运行程序时出现错误。\n");
    fprintf(stderr, "%s\n", s);
    fprintf(stderr, "Error number %x.\n", GetLastError());
    fprintf(stderr, "程序终止。\n");
    exit(1);
}
//   Code for the function VerifyFile 功能：验证数字签名
BOOL MyVerifyFile(
    PCHAR szSource,
    PCHAR szSignature)
{
    //   Declare and initialize local variables.
    FILE* hSource;
    FILE* hSignature;

    HCRYPTPROV hCryptProv; //CSP：钥匙容器
    HCRYPTKEY hKey;   //公钥对：包括配对的一个公钥和一个密钥
    HCRYPTKEY hPubKey;  //公钥对中的公钥
    HCRYPTHASH hHash;  //hash对象，用于对数据文件进行hash处理，得到hash值
    //公钥签名就是针对hash值进行签名，而不是原文件，
    //这是因为公钥处理的速度非常慢
    BYTE* pbSignature;
    DWORD dwSigLen;
    PBYTE pbBuffer;
    DWORD dwBufferLen;
    DWORD dwCount;
    //DWORD dwBlobLen;
    // Open source file.
    if (hSource = fopen(szSource, "rb"))
    {
        printf("\n源明文文件 %s 已打开。\n", szSource);
    }
    else
    {
        HandleError("\n打开源代码明文文件时出错!");
    }
    // Allocate memory.
    if (pbBuffer = (BYTE*)malloc(MAX_FILE_SIZE))
    {
        printf("已为缓冲区分配了内存。 \n");
    }
    else
    {
        HandleError("内存不足。\n");
    }
    //将源文件读入pbBuffer
    dwCount = fread(pbBuffer, 1, MAX_FILE_SIZE, hSource);
    if (ferror(hSource))
    {
        HandleError("读取明文错误!\n");
    }
    // Open signature file 读入签名文件（特殊处理：直接采用保留在内存中的签名来进行验证）
    if (hSignature = fopen(szSignature, "rb"))
    {
        printf("\n签名明文文件 %s 已打开。\n", szSignature);
    }
    else
    {
        HandleError("\n打开签名文件时出错!");
    }
    //--------------------------------------------------------------------
    // Allocate memory.
    if (pbSignature = (BYTE*)malloc(SIGNATURE_SIZE))
    {
        printf("已为缓冲区分配了内存。\n");
    }
    else
    {
        HandleError("内存不足。\n");
    }
    //将签名读入pbSignature
    dwSigLen = fread(pbSignature, 1, SIGNATURE_SIZE, hSignature);
    if (ferror(hSource))
    {
        HandleError("读取明文错误!\n");
    }
    //以下获得一个CSP句柄
    if (CryptAcquireContext(
                &hCryptProv,  //调用完成之后hCryptProv保存密钥容器的句柄
                NULL,    //NULL表示使用默认密钥容器，默认密钥容器名为用户登陆名
                NULL,
                PROV_RSA_FULL,
                0))
    {
        printf("已获取加密提供程序。\n");
    }
    else
    {
        if (CryptAcquireContext(
                    &hCryptProv,
                    NULL,
                    NULL,
                    PROV_RSA_FULL,
                    CRYPT_NEWKEYSET))   //创建密钥容器
        {
            //创建密钥容器成功，并得到CSP句柄
            printf("已经创建了一个新的密钥容器。\n");
        }
        else
        {
            HandleError("无法创建新的密钥容器。\n");
        }
    }
    //导入 pbKeyBlob 公钥（这个公钥与签名时所用的私钥配对，在签名时导出到pbKeyBlob中）
    if (CryptImportKey(
                hCryptProv,
                pbKeyBlob,
                dwBlobLen,
                0,
                0,
                &hPubKey))
    {
        printf("已导入密钥。\n");
    }
    else
    {
        HandleError("公钥导入失败。");
    }
    // Create a new hash object. 对原文件进行hash处理
    if (CryptCreateHash(
                hCryptProv,
                CALG_MD5,
                0,
                0,
                &hHash))
    {
        printf("已经重新创建了哈希对象。\n");
    }
    else
    {
        HandleError("CryptCreateHash 过程中出错。");
    }
    // Compute the cryptographic hash of the buffer.
    if (CryptHashData(
                hHash,
                pbBuffer,
                dwCount,
                0))
    {
        printf("The new has been created.\n");
    }
    else
    {
        HandleError("CryptHashData 期间发生错误。");
    }
    // Validate the digital signature. 验证数字签名是否正确
    if (CryptVerifySignature(
                hHash,
                pbSignature,
                dwSigLen,
                hPubKey,
                NULL,
                0))
    {
        printf("\n**********************************************\n");
        printf("签名已通过验证!\n");
        printf("\n**********************************************\n");
    }
    else
    {
        printf("\n**********************************************\n");
        printf("签名未验证!");
        printf("\n**********************************************\n");
    }
    // Close files.
    if (hSource)
        fclose(hSource);
    //if(hSignature)
    // fclose(hSignature);
    // Free memory to be used to store signature.
    if (pbSignature)
        free(pbSignature);
    // Destroy the hash object.
    if (hHash)
        CryptDestroyHash(hHash);
    // Release the provider handle.
    if (hCryptProv)
        CryptReleaseContext(hCryptProv, 0);
    return(TRUE);

}
//   Code for the function SignFile 功能：对文件进行数字签名
BOOL MySignFile(
    PCHAR szSource,
    PCHAR szDestination)
{
    //   Declare and initialize local variables.
    FILE* hSource;
    FILE* hDestination;

    HCRYPTPROV hCryptProv;
    HCRYPTKEY hKey;
    HCRYPTHASH hHash;

    BYTE* pbSignature;
    PBYTE pbBuffer;
    DWORD dwBufferLen;
    DWORD dwCount;
    DWORD dwSigLen;
    // Open source file.
    if (hSource = fopen(szSource, "rb"))
    {
        printf("\n源明文文件 %s 已打开。\n", szSource);
    }
    else
    {
        HandleError("\n打开源代码明文文件时出错!");
    }
    // Allocate memory.
    if (pbBuffer = (BYTE*)malloc(MAX_FILE_SIZE))
    {
        printf("已为缓冲区分配了内存。\n");
    }
    else
    {
        HandleError("内存不足。\n");
    }
    dwCount = fread(pbBuffer, 1, MAX_FILE_SIZE, hSource);
    if (ferror(hSource))
    {
        HandleError("读取明文错误!\n");
    }
    // Open destination file.
    if (hDestination = fopen(szDestination, "wb"))
    {
        printf("/n目标文件 %s 已打开。\n", szDestination);
    }
    else
    {
        HandleError("\n打开目标密文文件时出错!");
    }
    //以下获得一个CSP句柄
    if (CryptAcquireContext(
                &hCryptProv,
                NULL,    //NULL表示使用默认密钥容器，默认密钥容器名为用户登陆名
                NULL,
                PROV_RSA_FULL,
                0))
    {
        printf("已经获得了一个加密提供者。\n");
    }
    else
    {
        if (CryptAcquireContext(
                    &hCryptProv,
                    NULL,
                    NULL,
                    PROV_RSA_FULL,
                    CRYPT_NEWKEYSET))   //创建密钥容器
        {
            //创建密钥容器成功，并得到CSP句柄
            printf("已经创建了一个新的密钥容器。\n");
        }
        else
        {
            HandleError("无法创建新的密钥容器。\n");
        }
    }
    if (CryptGetUserKey(
                hCryptProv,                     // 我们已经得到的CSP句柄
                AT_SIGNATURE,                   // 这里想得到signature key pair
                &hKey))                         // 返回密钥句柄
    {
        printf("已准备好签名密钥。\n");
    }
    else     //取signature key pair错误
    {
        printf("无可用签名密钥。\n");
        if (GetLastError() == NTE_NO_KEY)   //密钥容器里不存在signature key pair
        {
            // 创建 signature key pair.
            printf("签名密钥不存在。\n");
            printf("创建签名密钥对。\n");
            if (CryptGenKey(
                        hCryptProv,  //CSP句柄
                        AT_SIGNATURE, //创建的密钥对类型为signature key pair
                        0,    //key类型，这里用默认值
                        &hKey))   //创建成功返回新创建的密钥对的句柄
            {
                printf("创建签名密钥对。\n");
            }
            else
            {
                printf("创建签名密钥时发生错误。\n");
            }
        }
        else
        {
            printf("一个错误，而不是 NTE_NO_KEY 获取签名/密钥。\n");
        }
    }
    // 导出公钥
    // Export the public key. Here the public key is exported to a
    // PUBLICKEYBOLB so that the receiver of the signed hash can
    // verify the signature. This BLOB could be written to a file and
    // sent to another user.
    if (CryptExportKey(
                hKey,
                NULL,
                PUBLICKEYBLOB,
                0,
                NULL,
                &dwBlobLen))
    {
        printf("确定公钥的 BLOB 的大小。\n");
    }
    else
    {
        HandleError("计算 BLOB 长度错误。");
    }
    // Allocate memory for the pbKeyBlob.
    if (pbKeyBlob = (BYTE*)malloc(dwBlobLen))
    {
        printf("已经为 BLOB 分配了内存。\n");
    }
    else
    {
        HandleError("内存不足。\n");
    }
    // Do the actual exporting into the key BLOB.
    if (CryptExportKey(
                hKey,
                NULL,
                PUBLICKEYBLOB,
                0,
                pbKeyBlob,
                &dwBlobLen))
    {
        printf("内容已写入 BLOB。\n");
    }
    else
    {
        HandleError("CryptExportKey 期间出错。");
    }
    //签名密钥已经准备完毕，公钥也已导出到 pbKeyBlob 中
    // Create the hash object.
    if (CryptCreateHash(
                hCryptProv,
                CALG_MD5,
                0,
                0,
                &hHash))
    {
        printf("创建的散列对象。\n");
    }
    else
    {
        HandleError("CryptCreateHash 过程中出错。");
    }
    if (CryptHashData(
                hHash,
                pbBuffer,
                dwCount,
                0))
    {
        printf("数据缓冲区已被散列。\n");
    }
    else
    {
        HandleError("CryptHashData 期间发生错误。\n");
    }
    //释放缓冲区
    if (pbBuffer)
        free(pbBuffer);
    pbBuffer = NULL;
    // Determine the size of the signature and allocate memory.
    dwSigLen = 0;
    if (CryptSignHash(
                hHash,
                AT_SIGNATURE,
                NULL,
                0,
                NULL,
                &dwSigLen))
    {
        printf("发现签名长度 %d。\n", dwSigLen);
    }
    else
    {
        HandleError("CryptSignHash 期间发生错误。");
    }
    // Allocate memory for the signature buffer.
    if (pbSignature = (BYTE*)malloc(dwSigLen))
    {
        printf("为签名分配的内存。\n");
    }
    else
    {
        HandleError("内存不足。");
    }
    // Sign the hash object.
    if (CryptSignHash(
                hHash,
                AT_SIGNATURE,
                NULL,
                0,
                pbSignature,
                &dwSigLen))
    {
        printf("pbSignature 是哈希签名。\n");
    }
    else
    {
        HandleError("CryptSignHash 期间发生错误。");
    }

    if (fwrite(pbSignature, 1, dwSigLen, hDestination) != dwSigLen)
        HandleError("将签名写入文件失败!");
    printf("散列对象已被销毁。\n");
    printf("该项目的签约阶段已经完成。\n\n");
    // Destroy session key.
    if (hKey)
        CryptDestroyKey(hKey);
    // Destroy the hash object.
    if (hHash)
        CryptDestroyHash(hHash);
    // Release the provider handle.
    if (hCryptProv)
        CryptReleaseContext(hCryptProv, 0);
    // Close files.
    if (hSource)
        fclose(hSource);
    if (hDestination)
        fclose(hDestination);
    return(TRUE);
}
//--------------------------------------------------------------------
//   Code for the function Decryptfile, which is called by main too
BOOL MyDecryptFile(
    PCHAR szSource,
    PCHAR szDestination,
    PCHAR szPassword)
{
    //   Declare and initialize local variables.
    FILE* hSource;
    FILE* hDestination;

    HCRYPTPROV hCryptProv;
    HCRYPTKEY hKey;
    HCRYPTHASH hHash;

    PBYTE pbBuffer;
    DWORD dwBlockLen;
    DWORD dwBufferLen;
    DWORD dwCount;
    // Open source file.
    if (hSource = fopen(szSource, "rb"))
    {
        printf("\n源明文文件 %s 已打开。\n", szSource);
    }
    else
    {
        HandleError("\n打开源代码明文文件时出错!");
    }
    // Open destination file.
    if (hDestination = fopen(szDestination, "wb"))
    {
        printf("\n目标文件 %s 已打开。\n", szDestination);
    }
    else
    {
        HandleError("\n打开目标密文文件时出错!");
    }

    //以下获得一个CSP句柄
    if (CryptAcquireContext(
                &hCryptProv,
                NULL,    //NULL表示使用默认密钥容器，默认密钥容器名为用户登陆名
                NULL,
                PROV_RSA_FULL,
                0))
    {
        printf("已获取加密提供程序。\n");
    }
    else
    {
        if (CryptAcquireContext(
                    &hCryptProv,
                    NULL,
                    NULL,
                    PROV_RSA_FULL,
                    CRYPT_NEWKEYSET))   //创建密钥容器
        {
            //创建密钥容器成功，并得到CSP句柄
            printf("已经创建了一个新的密钥容器。\n");
        }
        else
        {
            HandleError("无法创建新的密钥容器。\n");
        }

    }
    //--------------------------------------------------------------------
    // 创建一个会话密钥（session key）
    // 会话密钥也叫对称密钥，用于对称加密算法。
    // （注: 一个Session是指从调用函数CryptAcquireContext到调用函数
    //   CryptReleaseContext 期间的阶段。会话密钥只能存在于一个会话过程）
    //--------------------------------------------------------------------
    // Create a hash object.
    if (CryptCreateHash(
                hCryptProv,
                CALG_MD5,
                0,
                0,
                &hHash))
    {
        printf("已经创建了一个哈希对象。\n");
    }
    else
    {
        HandleError("CryptCreateHash 过程中出错!\n");
    }
    // 用输入的密码产生一个散列
    if (CryptHashData(
                hHash,
                (BYTE*)szPassword,
                strlen(szPassword),
                0))
    {
        printf("密码已添加到散列中。\n");
    }
    else
    {
        HandleError("CryptHashData 期间发生错误。\n");
    }
    // 通过散列生成会话密钥
    if (CryptDeriveKey(
                hCryptProv,
                ENCRYPT_ALGORITHM,
                hHash,
                KEYLENGTH,
                &hKey))
    {
        printf("加密密钥源自密码哈希。\n");
    }
    else
    {
        HandleError("CryptDeriveKey 期间出错!\n");
    }
    // Destroy the hash object.
    CryptDestroyHash(hHash);
    hHash = NULL;
    //--------------------------------------------------------------------
    //  The session key is now ready.
    //--------------------------------------------------------------------
    // 因为加密算法是按ENCRYPT_BLOCK_SIZE 大小的块加密的，所以被加密的
    // 数据长度必须是ENCRYPT_BLOCK_SIZE 的整数倍。下面计算一次加密的
    // 数据长度。
    dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE;
    // Determine the block size. If a block cipher is used,
    // it must have room for an extra block.
    if (ENCRYPT_BLOCK_SIZE > 1)
        dwBufferLen = dwBlockLen + ENCRYPT_BLOCK_SIZE;
    else
        dwBufferLen = dwBlockLen;
    // Allocate memory.
    if (pbBuffer = (BYTE*)malloc(dwBufferLen))
    {
        printf("已为缓冲区分配了内存。\n");
    }
    else
    {
        HandleError("内存不足。\n");
    }
    do
    {
        //--------------------------------------------------------------------
        // Read up to dwBlockLen bytes from the source file.
        dwCount = fread(pbBuffer, 1, dwBlockLen, hSource);
        if (ferror(hSource))
        {
            HandleError("读取明文错误!\n");
        }
        // 解密数据
        if (!CryptDecrypt(
                    hKey,   //密钥
                    0,    //如果数据同时进行散列和加密，这里传入一个散列对象
                    feof(hSource), //如果是最后一个被加密的块，输入TRUE.如果不是输.
                    //入FALSE这里通过判断是否到文件尾来决定是否为最后一块。
                    0,    //保留
                    pbBuffer,  //输入被加密数据，输出加密后的数据
                    &dwCount))   //输入被加密数据实际长度，输出加密后数据长度
        {
            HandleError("CryptEncrypt 期间发生错误。\n");
        }
        // 打印解密后的数据
        printf("解密后的数据：\n");
        for (DWORD i = 0; i < dwCount; ++i)
        {
            printf("%c", pbBuffer[i]);
        }
        printf("\n");
        // Write data to the destination file.
        fwrite(pbBuffer, 1, dwCount, hDestination);
        if (ferror(hDestination))
        {
            HandleError("写入密文错误。");
        }
    }
    while (!feof(hSource));
    // Close files.
    if (hSource)
        fclose(hSource);
    if (hDestination)
        fclose(hDestination);
    // Free memory.
    if (pbBuffer)
        free(pbBuffer);
    // Destroy session key.
    if (hKey)
        CryptDestroyKey(hKey);
    // Destroy hash object.
    if (hHash)
        CryptDestroyHash(hHash);
    // Release provider handle.
    if (hCryptProv)
        CryptReleaseContext(hCryptProv, 0);
    return(TRUE);

}
//   Code for the function EncryptFile called by main.
BOOL MyEncryptFile(
    PCHAR szSource,
    PCHAR szDestination,
    PCHAR szPassword)
{
    //   Declare and initialize local variables.
    FILE* hSource;
    FILE* hDestination;

    HCRYPTPROV hCryptProv;
    HCRYPTKEY hKey;
    HCRYPTHASH hHash;

    PBYTE pbBuffer;
    DWORD dwBlockLen;
    DWORD dwBufferLen;
    DWORD dwCount;
    // Open source file.
    if (hSource = fopen(szSource, "rb"))
    {
        printf("\n源明文文件 %s 已打开。\n", szSource);
    }
    else
    {
        HandleError("\n打开源代码明文文件时出错!");
    }
    // Open destination file.
    if (hDestination = fopen(szDestination, "wb"))
    {
        printf("\n目标文件 %s 已打开。\n", szDestination);
    }
    else
    {
        HandleError("\n打开目标密文文件时出错!");
    }

    //以下获得一个CSP句柄
    if (CryptAcquireContext(
                &hCryptProv,
                NULL,    //NULL表示使用默认密钥容器，默认密钥容器名为用户登陆名
                NULL,
                PROV_RSA_FULL,
                0))
    {
        printf("已获取加密提供程序。\n");
    }
    else
    {
        if (CryptAcquireContext(
                    &hCryptProv,
                    NULL,
                    NULL,
                    PROV_RSA_FULL,
                    CRYPT_NEWKEYSET))   //创建密钥容器
        {
            //创建密钥容器成功，并得到CSP句柄
            printf("已经创建了一个新的密钥容器。\n");
        }
        else
        {
            HandleError("无法创建新的密钥容器。\n");
        }

    }
    // 创建一个会话密钥（session key）
    // 会话密钥也叫对称密钥，用于对称加密算法。
    // （注: 一个Session是指从调用函数CryptAcquireContext到调用函数
    //   CryptReleaseContext 期间的阶段。会话密钥只能存在于一个会话过程）

    // Create a hash object.
    if (CryptCreateHash(
                hCryptProv,
                CALG_MD5,
                0,
                0,
                &hHash))
    {
        printf("已经创建了一个哈希对象。\n");
    }
    else
    {
        HandleError("CryptCreateHash 过程中出错!\n");
    }
    // 用输入的密码产生一个散列
    if (CryptHashData(
                hHash,
                (BYTE*)szPassword,
                strlen(szPassword),
                0))
    {
        printf("密码已添加到散列中。\n");
    }
    else
    {
        HandleError("CryptHashData 期间发生错误。\n");
    }
    // 通过散列生成会话密钥
    if (CryptDeriveKey(
                hCryptProv,
                ENCRYPT_ALGORITHM,
                hHash,
                KEYLENGTH,
                &hKey))
    {
        printf("加密密钥源自密码哈希。\n");
    }
    else
    {
        HandleError("CryptDeriveKey 期间出错!\n");
    }
    // Destroy the hash object.
    CryptDestroyHash(hHash);
    hHash = NULL;
    //  The session key is now ready.
    // 因为加密算法是按ENCRYPT_BLOCK_SIZE 大小的块加密的，所以被加密的
    // 数据长度必须是ENCRYPT_BLOCK_SIZE 的整数倍。下面计算一次加密的
    // 数据长度。
    dwBlockLen = 1000 - 1000 % ENCRYPT_BLOCK_SIZE;
    // Determine the block size. If a block cipher is used,
    // it must have room for an extra block.
    if (ENCRYPT_BLOCK_SIZE > 1)
        dwBufferLen = dwBlockLen + ENCRYPT_BLOCK_SIZE;
    else
        dwBufferLen = dwBlockLen;
    // Allocate memory.
    if (pbBuffer = (BYTE*)malloc(dwBufferLen))
    {
        printf("已为缓冲区分配了内存。\n");
    }
    else
    {
        HandleError("内存不足。\n");
    }
    // In a do loop, encrypt the source file and write to the source file.
    do
    {
        // Read up to dwBlockLen bytes from the source file.
        dwCount = fread(pbBuffer, 1, dwBlockLen, hSource);
        if (ferror(hSource))
        {
            HandleError("读取明文错误!\n");
        }
        // 加密数据
        if (!CryptEncrypt(
                    hKey,   //密钥
                    0,    //如果数据同时进行散列和加密，这里传入一个散列对象
                    feof(hSource), //如果是最后一个被加密的块，输入TRUE.如果不是输.
                    //入FALSE这里通过判断是否到文件尾来决定是否为最后一块。
                    0,    //保留
                    pbBuffer,  //输入被加密数据，输出加密后的数据
                    &dwCount,  //输入被加密数据实际长度，输出加密后数据长度
                    dwBufferLen))   //pbBuffer的大小。
        {
            HandleError("CryptEncrypt 期间发生错误。\n");
        }
        // 打印加密后的数据
        printf("加密后的数据：\n");
        for (DWORD i = 0; i < dwCount; ++i)
        {
            printf("%02X ", pbBuffer[i]);
        }
        printf("\n");
        // Write data to the destination file.
        fwrite(pbBuffer, 1, dwCount, hDestination);
        if (ferror(hDestination))
        {
            HandleError("写入密文错误。");
        }
    }
    while (!feof(hSource));
    // Close files.
    if (hSource)
        fclose(hSource);
    if (hDestination)
        fclose(hDestination);
    // Free memory.
    if (pbBuffer)
        free(pbBuffer);
    // Destroy session key.
    if (hKey)
        CryptDestroyKey(hKey);
    // Destroy hash object.
    if (hHash)
        CryptDestroyHash(hHash);
    // Release provider handle.
    if (hCryptProv)
        CryptReleaseContext(hCryptProv, 0);
    return(TRUE);
} // End of Encryptfile

void MyAddContentToFile(const char *fileName) {
    FILE *file = fopen(fileName, "a");  // 以追加模式打开文件
    if (!file) {
        HandleError("无法打开文件");
        return;
    }

    printf("请输入要添加的内容（以Ctrl+Z结束输入）:\n");

    char ch;
    while ((ch = getchar()) != EOF) {
        fputc(ch, file);
    }

    fclose(file);
    printf("内容已成功添加到文件 %s\n", fileName);
}

//   Begin main.
int main(int argc, char* argv[])
{
    int choice;
    CHAR szSource[100];
    CHAR szDestination[100];
    CHAR szPassword[100];
    CHAR outputFileName[100];

    printf("欢迎使用简易密码系统\n");
    printf("------------------------------------------------------------\n");
    printf("请选择操作:\n");
    printf("1. 加密文件\n");
    printf("2. 解密文件\n");
    printf("3. 签名文件\n");
    printf("4. 验签文件\n");
    printf("5. 新建文件或写入已有文件\n");
    printf("6. 退出\n");
    printf("------------------------------------------------------------\n");

    while(1){
        printf("\n请输入你的选择: ");
        scanf("%d", &choice);
        if (choice==1) {
            printf("\n输入要加密的文件名: ");
            scanf("%s", szSource);
            printf("输入输出文件的名称: ");
            scanf("%s", szDestination);
            printf("输入密码:");
            scanf("%s", szPassword);

            if (MyEncryptFile(szSource, szDestination, szPassword))
            {
                printf("文件 %s 的加密成功。\n", szSource);
                printf("加密的数据在文件 %s 中。\n", szDestination);
            }
            else
            {
                HandleError("/n加密文件错误!");
            }
        } else if (choice==2){
            printf("\n输入要解密的文件的名称: ");
            scanf("%s", szSource);
            printf("输入输出文件的名称: ");
            scanf("%s", szDestination);
            printf("输入密码:");
            scanf("%s", szPassword);

            if (MyDecryptFile(szSource, szDestination, szPassword))
            {
                printf("文件 %s 的解密成功。\n", szSource);
                printf("解密的数据在文件 %s 中。\n", szDestination);
            }
            else
            {
                HandleError("\n解密文件出错!");
            }
        } else if (choice==3){
            printf("\n输入要签名的文件名: ");
            scanf("%s", szSource);
            printf("输入签名文件名: ");
            scanf("%s", szDestination);

            if (MySignFile(szSource, szDestination))
            {
                printf("文件 %s 的签名成功。\n", szSource);
                printf("签名数据在文件 %s 中。\n", szDestination);
            }
            else
            {
                HandleError("\n签名文件时出错!");
            }
        } else if (choice==4){
                printf("\n输入需要验证的文件名: ");
                scanf("%s", szSource);
                printf("输入签名文件名: ");
                scanf("%s", szDestination);

                if (MyVerifyFile(szSource, szDestination))
                {
                    printf("文件 %s 的验证成功。\n", szSource);
                }
                else
                {
                    HandleError("\n验证失败。文件错误!");
                }
        } else if (choice == 5) {
            printf("请输入文件名: ");
            scanf("%s", outputFileName);
            MyAddContentToFile(outputFileName);
        } else if (choice == 6) {
            exit(0);
        } else {
            printf("无效的选择。\n");
        }
    }
    return 0;
}
