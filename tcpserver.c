#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>  // Added inclusion of unistd.h for close function

#define MAX 256
#define SERVER_HOST "localhost"
#define SERVER_IP "127.0.0.1"
#define SERVER_PORT 1234  // Defined SERVER_PORT here

struct sockaddr_in server_addr, client_addr;
int mysock, csock;

void server_init() {
    printf("================ server init =================\n");

    // 1. Create a TCP socket
    printf("1 : create a TCP socket\n");
    mysock = socket(AF_INET, SOCK_STREAM, 0);
    if (mysock < 0) {
        perror("socket call failed");
        exit(1);
    }

    // 2. Fill server_addr with server's IP and PORT#
    printf("2 : fill server_addr with server's IP and PORT#\n");
    memset((char*)&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY); // Use localhost
    server_addr.sin_port = htons(SERVER_PORT);

    // 3. Bind socket to server address
    printf("3 : bind socket to server address\n");
    int r = bind(mysock, (struct sockaddr*)&server_addr, sizeof(server_addr));
    if (r < 0) {
        perror("bind failed");
        exit(3);
    }

    printf("hostname = %s port = %d\n", SERVER_HOST, SERVER_PORT);
    printf("4 : server is listening ....\n");
    
    // 4. Listen for incoming connections
    listen(mysock, 5); // Queue length = 5

    printf("================== init done ===============\n");
}

int main() {
    int n;
    char line[MAX];

    server_init();

    while (1) {
        // Try to accept a client request
        printf("server: accepting new connection ....\n");
        socklen_t len = sizeof(client_addr);
        csock = accept(mysock, (struct sockaddr*)&client_addr, &len);
        if (csock < 0) {
            perror("server: accept error");
            exit(1);
        }

        printf("server: accepted a client connection from\n");
        printf("Client: IP=%s port=%d\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));

        // Processing loop: client_sock <== data ==> client
        while (1) {
            n = recv(csock, line, MAX, 0);
            if (n <= 0) {
                printf("server: client closed the connection, server loops\n");
                close(csock);
                break;
            }

            // Show the received line string
            printf("server: received n=%d bytes; line=%s\n", n, line);

            // Echo line to client
            n = send(csock, line, n, 0);
            printf("server: sent n=%d bytes; echo=%s\n", n, line);
        }
    }

    return 0;
}
