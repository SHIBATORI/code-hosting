#include <stdio.h>
int main()
{
    int pid;
    printf("THIS IS %d MY PARENT=%d\n", getpid(), getppid());
    pid = fork();   // fork syscall; parent returns child pid,
    if(pid){        // PARENTEXECUTES THIS PART
        printf("THIS IS PROCESs %d CHILD PID=%d\n", getpid(), pid);
    }
    else{           //child executes this part
        printf("this is process %d parent=8d\n", getpid(), getppid());
    }
}
