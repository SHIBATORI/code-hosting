#include <signal.h>
#include <stdio.h>
#include <sys/time.h> // 修正头文件名称

int count = 0;
struct itimerval t;

void timer_handler(int sig) { // 修正函数声明
    printf("timer_handler: signal=%d count=%d\n", sig, ++count); // 修正语法错误
    if (count >= 8) {
        printf("cancel timer\n"); // 修正语法错误
        t.it_value.tv_sec = 0;
        t.it_value.tv_usec = 0;
        setitimer(ITIMER_VIRTUAL, &t, NULL);
    }
}

int main() {
    struct itimerval timer;
    // 安装 timer_handler 作为 SIGVTALRM 信号处理函数
    signal(SIGVTALRM, timer_handler);
    
    // 配置定时器，在100毫秒后首次触发，之后每秒触发一次
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = 100000;
    timer.it_interval.tv_sec = 1;
    timer.it_interval.tv_usec = 0;

    // 启动一个虚拟定时器
    setitimer(ITIMER_VIRTUAL, &timer, NULL);
    printf("looping: enter Control-c to terminate\n");
    
    while (1);
    
    return 0;
}
