#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>

char in[128], out[128], buf[4096];
int bsize, count, skip, seek, trnc;
int records, bytes;

int parse(char *s)
{
    char cmd[128], parm[128];
    char *p = index(s,'=');
    s[p-s] = 0;
    strcpy(cmd,s);
    strcpy(parm,p+1);
    if(!strcmp(cmd,"if"))
        strcpy(in,parm);
    if(!strcmp(cmd,"of"))
        strcpy(out, parm);
    if(!strcmp(cmd,"bs")
        bsize = atoi(parm);
    if(!strcmp(cmd,"count"))
        count = atoi(parm);
    if(!strcmp(cmd, "skip"))
        skip = atoi(parm);
    if(!strcmp(cmd, "seek"))
        seek = atoi(parm);
    if(!strcmp(cmd,"conv")){
        if(!strcmp(parm, "notrunc"))
            trnc = 0;
    }
}

int main(int argc, char *argv[])
{
    int fd, gd, n, i;
    if(argc < 3){
        printf("Usage: a.out if of ....\n" ); exit (1);
    }
    in[0] = out[0] = 0;
    bsize = count = skip = seek = 0;
    trnc = 1;
    for(i=1; i<argc; i++){
        parse(argv[i]);
    }

    if(in[0]==0) || out [0]==0){
        printf("need in/out files\n"); exit(2);
    }
    if(bsize==0 || count==0){
        printf("need bsize and count\n"); exit(3);
    }
    
    if((fd = open(in, O_RDONLY)) < 0){
        printf("open %s error\n",in); exit(4);
    }
    if(skip) lseek(fd,skip*bsize,SEEK_SET);
    if (trnc)
        gd = open(out, O_WRONLY|O_CREAT|O_TRUNC);
    else
        gd = open(out, O_WRONLY|O_CREAT); 
    if(gd < 0){
        printf("open %s error\n",out); exit(5);
    }
    if(seek) lseek(gd,seek*bsize,SEEK_SET);

    records = bytes = 0;
    while(n = read(fd,buf,bsize)){
        write(gd,buf,n);
        records++; bytes += n;
        count--;
        if(count==0) break;
    }
    printf("records=%d bytes=%d copied\n",records,bytes);
}