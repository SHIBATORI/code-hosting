#include <stdio.h>
#include <stdlib.h>
#include <string.h>  // Fixed spelling of string.h
#include <sys/socket.h>
#include <netinet/in.h>  // Fixed inclusion of the correct header file
#define BUFLEN 256  // Fixed the definition of BUFLEN
#define PORT 1234   // Fixed the definition of PORT

char line[BUFLEN];
struct sockaddr_in me, client;
int sock, rlen, clen = sizeof(client);

int main() {
    printf("1. create a UDP socket\n");
    sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);  // Fixed spelling of IPPROTO_UDP
    printf("2. fill me with server address and port number\n");
    memset((char*)&me, 0, sizeof(me));  // Fixed memset syntax and size parameter
    me.sin_family = AF_INET;
    me.sin_port = htons(PORT);
    me.sin_addr.s_addr = htonl(INADDR_ANY);  // Use INADDR_ANY for localhost
    printf("3. bind socket to server IP and port\n");
    bind(sock, (struct sockaddr*)&me, sizeof(me));  // Fixed cast in bind function
    printf("4. wait for datagram\n");

    while (1) {
        memset(line, 0, BUFLEN);
        printf("UDP server: waiting for datagram\n");
        // recvfrom() gets client IP, port in sockaddr_in client
        rlen = recvfrom(sock, line, BUFLEN, 0, (struct sockaddr*)&client, &clen);  // Fixed cast in recvfrom function
        printf("received a datagram from [host:port] = [%s:%d]\n", inet_ntoa(client.sin_addr), ntohs(client.sin_port));
        printf("rlen=%d: line=%s\n", rlen, line);
        printf("send reply\n");
        sendto(sock, line, rlen, 0, (struct sockaddr*)&client, clen);  // Fixed cast in sendto function
    }
}
