#! /bin/bash

cpf2f()
{
	if [ ! -e $1 ]; then
		echo no such source file $1
		return 1
	fi
	if [ $1 -ef $2 ]; then
		echo "never copy a file to itself"
	return 1
	fi
	if [ -L $1 ]; then
		echo "copying symlink $1"
		link=$(readlink $1)
			ln -s $link $2
			return 0
	fi
	echo "copying $1 to $2"
	cp $1 $2 2> /dev/null
}

cpf2d()
{
	newfile=$2/$(basename $1)
	cpf2f $1 $newfile
}

cpd2d()
{
	src=$1
	dest=$2
	
	if [ ! -d $dest ]; then
		echo "Destination $dest is not a directory."
		return 1
	fi
	newdir=$dest/$(basename $src)
	mkdir -p $newdir
	for file in $src/*; do
		if [ -f $file ]; then
			cpf2f $file $newdir/$(basename $file)
		elif [ -d $file ]; then
			cpd2d $file $newdir
		fi
	done
}
#******************entry point of myrcp******************
#case analysis;
#for each f1 to fn-1 call cpf2f() or cpf2d() or cpd2d()
myrcp()
{
	if [ $# -lt 2 ]; then
		echo "use myrcp f1 f2 ... fn"
		return 1
	fi

	num_args=$#
	dest=${!num_args}

	if [ ! -d $dest ]; then
		if [ $# -eq 2 ]; then
			dest=$2
		else
			echo "Destination $dest does not exist."
			return 1
		fi
	fi

	for ((i=1; i<num_args;i++)); do
		src=${!i}
		if [ ! -e $src ]; then
			echo "no such source file $src"
			continue
		fi

		if [ -d $dest ]; then
			if [ -d $src ]; then
				cpd2d $src $dest
			else
				cpf2d $src $dest
			fi
		else
			cpf2f $src $dest
		fi
	done		
}

myrcp $@