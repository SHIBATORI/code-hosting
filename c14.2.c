#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mysql/mysql.h>

#define N 5

int id[] = {1001, 1002, 2001, 2002, 2003};
char name[][20] = {"Baker", "Miller", "Smith", "Walton", "Zach"};
int score[] = {65, 50, 75, 95, 85};

MYSQL *con;

void error() {
    printf("Error: %d %s\n", mysql_errno(con), mysql_error(con));
    mysql_close(con);
    exit(1);
}

int main(int argc, char *argv[]) {
    int i;
    char buf[1024]; // Used to create commands for MySQL
    
    con = mysql_init(NULL); // MySQL server will allocate and init con
    if (con == NULL) {
        error();
    }

    printf("Connect to MySQL server on localhost using database testdb\n");
    if (mysql_real_connect(con, "localhost", "root", "1234", "testdb", 0, NULL, 0) == NULL) {
        error();
    }

    printf("Connected to server OK\n");
    printf("Drop students table if exists\n");
    if (mysql_query(con, "DROP TABLE IF EXISTS students")) {
        error();
    }

    printf("Create students table in testdb\n");
    if (mysql_query(con, "CREATE TABLE students(Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, name CHAR(20) NOT NULL, score INT)")) {
        error();
    }

    printf("Insert student records into students table\n");
    for (i = 0; i < N; i++) {
        printf("id=%d name=%s score=%d\n", id[i], name[i], score[i]);
        sprintf(buf, "INSERT INTO students VALUES(%d, '%s', %d);", id[i], name[i], score[i]);
        if (mysql_real_query(con, buf, strlen(buf))) {
            error();
        }
    }

    printf("All done\n");
    mysql_close(con);

    return 0;
}
