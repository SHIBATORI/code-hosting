#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h> 
char *line = "testing named pipe";
int main() {
    int fd;
    if (mknod("mypipe", S_IFIFO | 0666, 0) == -1) {
        perror("Error creating named pipe");
        return 1;
    }
    fd = open("mypipe", O_WRONLY);
    if (fd == -1) {
        perror("Error opening named pipe for write");
        return 1;
    }
    write(fd, line, strlen(line));
    close(fd);
    return 0;
}