#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <openssl/evp.h>

void sm3_hash(const unsigned char *data, size_t data_len, unsigned char *digest) {
    EVP_MD_CTX *mdctx;
    const EVP_MD *md;
    unsigned int md_len;

    md = EVP_sm3(); // 获取SM3摘要算法的消息摘要对象

    mdctx = EVP_MD_CTX_new(); // 创建消息摘要上下文对象
    EVP_DigestInit_ex(mdctx, md, NULL); // 初始化消息摘要上下文
    EVP_DigestUpdate(mdctx, data, data_len); // 更新消息摘要上下文，添加待摘要的数据
    EVP_DigestFinal_ex(mdctx, digest, &md_len); // 计算消息摘要
    EVP_MD_CTX_free(mdctx); // 释放消息摘要上下文对象
}

int main() {
    const unsigned char data[] = "Hello, World!";
    unsigned char digest[EVP_MAX_MD_SIZE]; // 用于存储消息摘要结果
    int i;

    sm3_hash(data, strlen((char *)data), digest); // 计算SM3摘要

    printf("SM3 Digest: ");
    for (i = 0; i < EVP_MD_size(EVP_sm3()); i++) {
        printf("%02x", digest[i]); // 打印摘要结果
    }
    printf("\n");

    return 0;
}