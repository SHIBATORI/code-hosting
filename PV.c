#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <semaphore.h>
#include <pthread.h>

#define BUFFER_SIZE 5
#define MAX_PRODUCE_COUNT 20
#define MAX_CONSUME_COUNT 20

int buffer[BUFFER_SIZE];
sem_t empty, full;
pthread_mutex_t mutex;
int produce_count = 0;
int consume_count = 0;

void *producer(void *arg) {
    while (produce_count < MAX_PRODUCE_COUNT) {
        sem_wait(&empty);
        pthread_mutex_lock(&mutex);

        // Produce item and put it in the buffer
        buffer[rand() % BUFFER_SIZE] = produce_count + 1; // Range: 1-20
        printf("Produced item: %d\n", produce_count + 1);
        produce_count++;

        pthread_mutex_unlock(&mutex);
        sem_post(&full);
    }
    pthread_exit(NULL);
}

void *consumer(void *arg) {
    while (consume_count < MAX_CONSUME_COUNT) {
        sem_wait(&full);
        pthread_mutex_lock(&mutex);

        // Consume item from the buffer
        int item = buffer[rand() % BUFFER_SIZE];
        printf("Consumed item: %d\n", item);
        consume_count++;

        pthread_mutex_unlock(&mutex);
        sem_post(&empty);
    }
    pthread_exit(NULL);
}

int main() {
    pthread_t producer_thread, consumer_thread;

    // Initialize semaphores and mutex
    sem_init(&empty, 0, BUFFER_SIZE);
    sem_init(&full, 0, 0);
    pthread_mutex_init(&mutex, NULL);

    // Create producer and consumer threads
    pthread_create(&producer_thread, NULL, producer, NULL);
    pthread_create(&consumer_thread, NULL, consumer, NULL);

    // Wait for threads to finish
    pthread_join(producer_thread, NULL);
    pthread_join(consumer_thread, NULL);

    // Clean up semaphores and mutex
    sem_destroy(&empty);
    sem_destroy(&full);
    pthread_mutex_destroy(&mutex);

    return 0;
}
