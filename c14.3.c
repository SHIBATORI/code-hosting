#include <mysql/mysql.h>
#include <stdio.h>
#include <string.h>

#define N 5

int id[] = {1001, 1002, 2001, 2002, 2003};
char name[][20] = {"Baker", "Miller", "Smith", "Walton", "Zach"};
int score[] = {65, 50, 75, 95, 85};
char grade[] = {'D', 'F', 'C', 'A', 'B'};

MYSQL *con;

void error() {
    printf("Error: %d %s\n", mysql_errno(con), mysql_error(con));
    mysql_close(con);
    exit(1);
}

int main(int argc, char **argv) {
    int i, ncols;
    MYSQL_ROW row;
    MYSQL_RES *result;
    MYSQL_FIELD *column;
    char buf[1024];

    con = mysql_init(NULL);
    if (con == NULL) {
        error();
    }

    printf("Connect to MySQL server on localhost using database cs360\n");
    if (mysql_real_connect(con, "localhost", "root", "1234", "testdb", 0, NULL, 0) == NULL) {
        error();
    }

    printf("Connected to server OK\n");

    printf("Drop students table if exists\n");
    if (mysql_query(con, "DROP TABLE IF EXISTS students")) {
        error();
    }

    printf("Create students table in testdb\n");
    if (mysql_query(con, "CREATE TABLE students(Id INT NOT NULL PRIMARY KEY AUTO_INCREMENT, name CHAR(20) NOT NULL, score INT, grade CHAR(2))")) {
        error();
    }

    printf("Insert student records into students table\n");
    for (i = 0; i < N; i++) {
        printf("id=%d name=%s score=%d grade=%c\n", id[i], name[i], score[i], grade[i]);
        sprintf(buf, "INSERT INTO students VALUES (%d, '%s', %d, '%c');", id[i], name[i], score[i], grade[i]);
        if (mysql_real_query(con, buf, strlen(buf))) {
            error();
        }
    }

    printf("Retrieve query results\n");
    mysql_query(con, "SELECT * FROM students");
    result = mysql_store_result(con);
    ncols = mysql_num_fields(result);
    printf("Number of columns = %d\n", ncols);

    for (i = 0; i < ncols; i++) {
        column = mysql_fetch_field(result);
        printf("Column no.%d name = %s\n", i + 1, column->name);
    }

    mysql_query(con, "SELECT * FROM students");
    result = mysql_store_result(con);
    ncols = mysql_num_fields(result);
    printf("Columns numbers = %d\n", ncols);

    while ((row = mysql_fetch_row(result))) {
        for (i = 0; i < ncols; i++) {
            printf("%s ", row[i]);
        }
        printf("\n");
    }

    printf("All done\n");
    mysql_close(con);

    return 0;
}
