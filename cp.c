#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#define BLKSIZE 4096
int main(int argc, char *argv[ ])
{
    int fd,gd,n,total=0;
    char buf[BLKSIZE];
    if(argc < 3) exit(1);
    if((fd = open(argv[1],O_RDONLY)) < 0)
        exit(2);
    if((gd = open(argv[2],O_WRONLY|O_CREAT)) < 0)
        exit(3);
    while(n = read(fd,buf,BLKSIZE)){
        write(gd,buf,n);
        total += n;
    }
    printf("total bytes copied=%d\n", total);
    close(fd); close(gd);
}