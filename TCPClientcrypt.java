package exp32;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.Base64;
import javax.crypto.Cipher;
import java.io.*;
import java.net.*;

public class TCPClientcrypt {
    public static void main(String[] args) {
        try {
            Socket socket = new Socket("192.168.109.23", 2048);

            // Setup streams for communication
            BufferedReader consoleReader = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);

            // Manually input username and password
            System.out.print("Enter username: ");
            String username = consoleReader.readLine();
            System.out.print("Enter password: ");
            String password = consoleReader.readLine();

            // Send username and password for authentication
            writer.println(username);
            writer.println(password);

            // Check authentication result from server
            String authenticationResult = reader.readLine();
            if ("Authentication failed".equals(authenticationResult)) {
                System.out.println("Authentication failed. Closing connection.");
                socket.close();
                return;
            }
            
            String welcomeMessage = reader.readLine();
            System.out.println(welcomeMessage);
            
            String replyMessage = username + " has connected to server.";
            writer.println(replyMessage);
            
            // 生成RSA密钥对
            KeyPair keyPair = generateRSAKeyPair();

            // 获取公钥和私钥
            PublicKey publicKey = keyPair.getPublic();
            PrivateKey privateKey = keyPair.getPrivate();
            //System.out.println("Public Key: " + publicKey);
            //System.out.println("private Key: " + privateKey);
            // 发送公钥到服务器
            sendPublicKeyToServer(publicKey, writer);
            
            // Keep the connection open for multiple messages
            while (true) {
                // Manually input message
                System.out.print("Enter message (or type 'exit' to close): ");
                String message = consoleReader.readLine();
                
                if ("exit".equalsIgnoreCase(message)) {
                    // Close the connection
                    socket.close();
                    break;
                }
                
                // 加密
                String encryptedText = encrypt(message, privateKey);
                System.out.println("Encrypted Text: " + encryptedText);
                
                // Send message to server
                writer.println(encryptedText);

                // Receive response from server
                String response = reader.readLine();
                System.out.println("Received response from server: " + response);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
	// 生成RSA密钥对
    private static KeyPair generateRSAKeyPair() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
        keyPairGenerator.initialize(2048); // 使用2048位密钥
        return keyPairGenerator.generateKeyPair();
    }
    // 使用RSA私钥加密字符串
    private static String encrypt(String input, PrivateKey privateKey) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, privateKey);
        byte[] encryptedBytes = cipher.doFinal(input.getBytes());
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }
    // 发送公钥到服务器
    private static void sendPublicKeyToServer(PublicKey publicKey, PrintWriter writer) {
        try {
            // 将公钥转换为字符串，并发送到服务器
            String publicKeyString = Base64.getEncoder().encodeToString(publicKey.getEncoded());
            writer.println(publicKeyString);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
