#include <stdio.h>
#include <stdlib.h>
#include <mysql/mysql.h>

MYSQL *con; // connection object pointer

void error() {
    printf("Error: %u %s\n", mysql_errno(con), mysql_error(con));
    mysql_close(con);
    exit(1);
}

int main(int argc, char *argv[]) {
    con = mysql_init(NULL); // will allocate and initialize it
    if (con == NULL) {
        error();
    }

    printf("Connect to MySQL server on localhost using database cs360\n");
    if (mysql_real_connect(con, "localhost", "root", "1234", "testdb", 0, NULL, 0) == NULL) {
        error();
    }

    printf("Connection to server OK\n");

    printf("Drop students table if exists\n");
    if (mysql_query(con, "DROP TABLE IF EXISTS students")) {
        error();
    }

    printf("Create students table in testdb\n");
    if (mysql_query(con, "CREATE TABLE students(rd INT NOT NULL PRIMARY KEY AUTO_INCREMENT, name CHAR(20) NOT NULL, score INT)")) {
        error();
    }

    printf("Insert student records into students table\n");
    if (mysql_query(con, "INSERT INTO students VALUES(1001, 'Baker', 50)")) {
        error();
    }
    if (mysql_query(con, "INSERT INTO students VALUES(1002, 'Miller', 65)")) {
        error();
    }
    if (mysql_query(con, "INSERT INTO students VALUES(2001, 'Miller', 75)")) {
        error();
    }
    if (mysql_query(con, "INSERT INTO students VALUES(2002, 'Smith', 85)")) {
        error();
    }

    printf("All done\n");
    mysql_close(con);

    return 0;
}
