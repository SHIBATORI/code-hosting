#include <stdio.h>
#include <string.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/err.h>

void handleErrors(void)
{
    ERR_print_errors_fp(stderr);
    abort();
}

int main(void)
{
    OpenSSL_add_all_algorithms();
    ERR_load_crypto_strings();

    // 生成随机key和iv
    unsigned char key[16];
    unsigned char iv[16];
    if (RAND_bytes(key, sizeof(key)) != 1 || RAND_bytes(iv, sizeof(iv)) != 1)
        handleErrors();

    // 打印生成的key和iv
    printf("Generated Key: ");
    for (int i = 0; i < sizeof(key); ++i)
    {
        printf("%02x", key[i]);
    }
    printf("\n");

    printf("Generated IV: ");
    for (int i = 0; i < sizeof(iv); ++i)
    {
        printf("%02x", iv[i]);
    }
    printf("\n");

    const char *plaintext = "Hello, 20211420!";
    unsigned char ciphertext[128];

    // 初始化加密上下文
    EVP_CIPHER_CTX *ctx = EVP_CIPHER_CTX_new();
    if (!ctx)
        handleErrors();

    // 使用生成的key和iv初始化SM4加密
    if (1 != EVP_EncryptInit_ex(ctx, EVP_sm4_cbc(), NULL, key, iv))
        handleErrors();

    int len;
    int ciphertext_len;

    // 加密明文
    if (1 != EVP_EncryptUpdate(ctx, ciphertext, &len, (unsigned char *)plaintext, strlen(plaintext)))
        handleErrors();
    ciphertext_len = len;

    // 完成加密
    if (1 != EVP_EncryptFinal_ex(ctx, ciphertext + len, &len))
        handleErrors();
    ciphertext_len += len;

    // 清理加密上下文
    EVP_CIPHER_CTX_free(ctx);

    // 打印密文
    printf("Ciphertext: ");
    for (int i = 0; i < ciphertext_len; ++i)
    {
        printf("%02x", ciphertext[i]);
    }
    printf("\n");

    // 初始化解密上下文
    ctx = EVP_CIPHER_CTX_new();
    if (!ctx)
        handleErrors();

    unsigned char decryptedtext[128];

    // 使用生成的key和iv初始化SM4解密
    if (1 != EVP_DecryptInit_ex(ctx, EVP_sm4_cbc(), NULL, key, iv))
        handleErrors();

    int decryptedtext_len;

    // 解密密文
    if (1 != EVP_DecryptUpdate(ctx, decryptedtext, &decryptedtext_len, ciphertext, ciphertext_len))
        handleErrors();

    // 完成解密
    if (1 != EVP_DecryptFinal_ex(ctx, decryptedtext + decryptedtext_len, &len))
        handleErrors();
    decryptedtext_len += len;

    // 清理解密上下文
    EVP_CIPHER_CTX_free(ctx);

    // 打印解密后的文本
    printf("Decrypted Text: ");
    // 使用fwrite输出包含零字节的二进制数据
    fwrite(decryptedtext, 1, decryptedtext_len, stdout);
    printf("\n");

    // 清理OpenSSL
    EVP_cleanup();
    ERR_free_strings();

    return 0;
}

