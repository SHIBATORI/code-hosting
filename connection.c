#include <stdio.h>
#include <stdlib.h>
#include <my_global.h>
#include <mysql.h>

int main(int argc, char *argv[])
{
    // 1. 定义一个MySQL连接对象
    MYSQL con;

    // 2. 初始化连接对象
    if (mysql_init(&con)) {   // 返回对象地址
        printf("Connection handle initialized\n");
    } else {
        printf("Connection handle initialization failed\n");
        exit(1);
    }

    // 3. 连接到本地MySQL服务器
    if (mysql_real_connect(&con, "localhost", "root", "1234", "testdb", 3306, NULL, 0)) {
        printf("Connection to remote MySQL server OK\n");
    } else {
        printf("Connection to remote MySQL failed\n");
        exit(1);
    }

    // 4. 当操作完成后关闭连接
    mysql_close(&con);
    printf("Connection closed\n");

    return 0;
}
