#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h> // 包含 time.h 头文件以使用 ctime 函数

struct timeval t;

int main()
{
    gettimeofday(&t, NULL);
    printf("sec=%ld usec=%ld\n", t.tv_sec, t.tv_usec);
    printf("%s", ctime(&t.tv_sec)); // 将时间作为字符串直接打印
    return 0; // 添加返回语句表示程序成功执行
}
