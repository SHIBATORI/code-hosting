/****************显示根索引节点信息****************/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <ext2fs/ext2_fs.h>
#include <time.h> // 添加包含 time.h 的头文件

#define BLKSIZE 1024
typedef struct ext2_group_desc GD;
typedef struct ext2_super_block SUPER;
typedef struct ext2_inode INODE;
typedef struct ext2_dir_entry_2 DIR;
SUPER *sp;
GD *gp;
INODE *ip;
DIR *dp;
char buf[BLKSIZE];
int fd, firstdata, inodesize, blksize, iblock;
char *dev = "mydisk";

int get_block(int fd, int blk, char *buf)
{
    lseek(fd, blk * BLKSIZE, SEEK_SET);
    return read(fd, buf, BLKSIZE);
}

int inode(char *dev)
{
    int i;
    fd = open(dev, O_RDONLY); // 移除 < 0 的错误判断
    if (fd < 0)
    {
        printf("open failed\n");
        exit(1);
    }
    get_block(fd, 1, buf);
    sp = (SUPER *)buf;
    firstdata = sp->s_first_data_block;
    inodesize = sp->s_inode_size;
    blksize = 1024 * (1 << sp->s_log_block_size);
    printf("first_data_block=%d block_size=%d inodesize=%d\n",
           firstdata, blksize, inodesize);
    get_block(fd, (firstdata + 1), buf);
    gp = (GD *)buf;
    printf("bmap_block=%d imap_block=%d inodes_table=%d ",
           gp->bg_block_bitmap,
           gp->bg_inode_bitmap,
           gp->bg_inode_table,
           gp->bg_free_blocks_count);
    printf("free_blocks=%d free_inodes=%d\n",
           gp->bg_free_inodes_count,
           gp->bg_used_dirs_count); // 修正变量名错误
    iblock = gp->bg_inode_table;
    printf("root inode information:\n");
    printf("---------------------\n"); // 修正输出格式
    get_block(fd, iblock, buf);
    ip = (INODE *)buf + 1;
    printf("mode=%4x ", ip->i_mode);
    printf("uid=%d gid=%d\n", ip->i_uid, ip->i_gid);
    printf("size=%d\n", ip->i_size);
    printf("ctime=%s", ctime((time_t *)&ip->i_ctime)); // 强制类型转换为 time_t*
    printf("links=%d\n", ip->i_links_count);
    for (i = 0; i < 15; i++)
    {
        if (ip->i_block[i])
            printf("i_block[%d]=%d\n", i, ip->i_block[i]);
    }
    close(fd); // 关闭文件描述符
}

int main(int argc, char *argv[])
{
    if (argc > 1)
        dev = argv[1];
    inode(dev);
}
