import tkinter as tk
from tkinter import Label, Entry, Button, Text, Scrollbar
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import scipy.stats as stats
import os

class LinearCongruentialGenerator:
    def __init__(self, seed, a, c, m):
        self.state = seed
        self.a = a
        self.c = c
        self.m = m

    def generate(self):
        self.state = (self.a * self.state + self.c) % self.m
        return self.state

class RandomNumberGeneratorApp:
    def __init__(self, master):
        self.master = master
        self.master.title("Random Number Generator")

        # 输入控件
        self.label_num = Label(master, text="Number of random numbers:")
        self.label_num.pack()

        self.entry_num = Entry(master)
        self.entry_num.pack()

        self.label_range = Label(master, text="Range (comma separated, e.g., 1,100):")
        self.label_range.pack()

        self.entry_range = Entry(master)
        self.entry_range.pack()

        # 输入控件 - 添加用于seed值的文本框
        self.label_seed = Label(master, text="Seed value:")
        self.label_seed.pack()

        self.entry_seed = Entry(master)
        self.entry_seed.pack()

        # 生成按钮
        self.generate_button = Button(master, text="Generate Random Numbers", command=self.generate_numbers)
        self.generate_button.pack()

        # 随机数显示文本框和滚动条
        self.text_display = Text(master, height=20, width=30)
        self.text_display.pack(side=tk.LEFT, fill=tk.Y)

        self.scrollbar = Scrollbar(master, command=self.text_display.yview)
        self.scrollbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.text_display.config(yscrollcommand=self.scrollbar.set)

        # 图像显示
        self.canvas = None

    def generate_numbers(self):
        filename = 'random_numbers.txt'

        num_count = int(self.entry_num.get())
        range_str = self.entry_range.get()
        range_start, range_end = map(int, range_str.split(','))

        # 获取seed值和c值
        seed_value = int(self.entry_seed.get())

        lcg = LinearCongruentialGenerator(seed=seed_value, a=1664525, c=1013904223, m=2 ** 32)
        random_numbers = [lcg.generate() % (range_end - range_start + 1) + range_start for _ in range(num_count)]

        hex_numbers = [hex(num) for num in random_numbers]
        self.save_to_txt(filename, random_numbers, hex_numbers)
        self.clear_display()
        self.display_numbers(hex_numbers)
        self.plot_histogram(random_numbers)

        # 进行频率检验
        num_bins = 10  # 调整区间数量
        p_value = self.frequency_test(random_numbers, num_bins)
        print(f"\n卡方检验 p-value: {p_value}")

        # 计算数字特征
        average = np.mean(random_numbers)
        var = np.var(random_numbers)
        standard = np.std(random_numbers)

        # 计算数字分布
        count1 = np.sum((np.array(random_numbers) >= range_start) & (np.array(random_numbers) <= average / 2.0))
        count2 = np.sum((average / 2.0 <= np.array(random_numbers)) & (np.array(random_numbers) <= average))
        count3 = np.sum((average <= np.array(random_numbers)) & (np.array(random_numbers) <= average * 3.0 / 2.0))
        count4 = np.sum((average * 3.0 / 2.0 <= np.array(random_numbers)) & (np.array(random_numbers) <= range_end))

        # 打印数字特征
        print(f"平均数为  {average:.2f}")
        print(f"处于 {range_start} — {average / 2.0} 之间的数有 {count1} 个")
        print(f"处于 {average / 2.0} — {average} 之间的数有 {count2} 个")
        print(f"处于 {average} — {average * 3.0 / 2.0} 之间的数有 {count3} 个")
        print(f"处于 {average * 3.0 / 2.0} — {range_end} 之间的数有 {count4} 个")
        print(f"方差为    {var:.2f}")
        print(f"标准差为  {standard:.2f}")

        # 将分析结果写入文件
        with open(filename, 'a', encoding='utf-8') as file:
            file.write(f"\n卡方检验 p-value: {p_value}\n")
            file.write(f"平均数为  {average:.2f}\n")
            file.write(f"处于 {range_start} — {average / 2.0} 之间的数有 {count1} 个\n")
            file.write(f"处于 {average / 2.0} — {average} 之间的数有 {count2} 个\n")
            file.write(f"处于 {average} — {average * 3.0 / 2.0} 之间的数有 {count3} 个\n")
            file.write(f"处于 {average * 3.0 / 2.0} — {range_end} 之间的数有 {count4} 个\n")
            file.write(f"方差为    {var:.2f}\n")
            file.write(f"标准差为  {standard:.2f}\n")

        # 在这里可以继续进行其他的统计检验或分析

    def frequency_test(self, random_numbers, num_bins):
        # 统计随机数落在各区间的频率
        observed, _ = np.histogram(random_numbers, bins=num_bins)

        # 期望的频率（假设均匀分布）
        expected = np.full_like(observed, fill_value=len(random_numbers) / num_bins)

        # 进行卡方检验
        _, p_value = stats.chisquare(observed, f_exp=expected)

        return p_value

    def clear_display(self):
        self.text_display.delete(1.0, tk.END)

    def display_numbers(self, numbers):
        for number in numbers:
            self.text_display.insert(tk.END, f"{number}\n")

    def plot_histogram(self, numbers):
        if self.canvas:
            self.canvas.get_tk_widget().destroy()

        fig, ax = plt.subplots()
        ax.hist(numbers, bins=20, density=True, alpha=0.7, color='blue')
        ax.set_title('Histogram of Generated Random Numbers')
        ax.set_xlabel('Value')
        ax.set_ylabel('Frequency')
        plt.savefig('histogram.png')

        self.canvas = FigureCanvasTkAgg(fig, master=self.master)
        self.canvas.draw()
        self.canvas.get_tk_widget().pack(side=tk.RIGHT, fill=tk.BOTH, expand=True)

    def save_to_txt(self, filename, decimal_numbers, hex_numbers):
        # 如果文件存在，先删除
        if os.path.exists(filename):
            os.remove(filename)

        with open(filename, 'a', encoding='utf-8') as file:
            for decimal, hex_num in zip(decimal_numbers, hex_numbers):
                file.write(f"{decimal} ——> {hex_num}\n")

if __name__ == "__main__":
    root = tk.Tk()
    app = RandomNumberGeneratorApp(root)
    root.mainloop()
