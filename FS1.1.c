#include "type.h"
#include "util.c"

// global variables
MINODE minode[NMINODES], *root;
MTABLE mtable[NMTABLE];
PROC proc[NPROC], *running;
int ninode, nblocks, bmap, imap, ibiock;
int dev;
char gline[25], *name[16];	// tokenized component string strings
int nname;			// number of component strings
char *rootdev = "mydisk";	// default root_device

int fs_init()
{
	int i, j;
	for(i=0; i<NMINODES; i++)	// initialize all minodes as FREE
		minode[i].refCount = 0;
	for(i=0; i<NMOUNT; i++)		// initialize mtable entries as FREE
		mtable[i].dev = 0;
	for (i=0; i<NPROC; i++){	// initialize PROCs
		proc[i].status = READY;	//reday to run
		proc[i].pid = i;		//pid = 0 to NPROC-1
		proc[i].uid = i;		//P0 is a superuser process
		for(j=0; j<NFD; j++)
			proc[i].fd[j] = 0;	//all file descriptors are NULL
		proc[i].next = &proc[i+1];
	}
	proc[NPROC-1].next = &proc[0];	//circular list
	running = &proc[0];				//P0 runs first
}

int mount_root (char *rootdev)		// mount root file system
{
	int i;
	MTABLE *mp;
	SUPER *sp;
	GD *gp;
	char buf[BLKSIZE];

	dev = open(rootdev, O_RDWR);
	if(dev < 0){
		printf("panic:can't open root device\n");
		exit(1);
	}
	/* get super block of rootdev */
	get_block(dev, 1, buf);
	sp = (SUPER *)buf;
	/* check magic number */
	if(sp->s_magic != SUPER_MAGIC){
	printf ("super magic=%x:%s is not an EXT2 filesysln",
			sp->s_magic, rootdev) ;
	exit (0);
	}
	// fill mount table mtable[0] with rootdev information
	mp = &mtable[0];	// use mtable[0]
	mp->dev = dev;
	// copy super block info into mtable[0]
	ninodes = mp->ninodes = sp->s_inodes_count;
	nblocks = mp->nblocks = sp->s_blocks_count;
	strcpy(mp->devName, rootdev);
	strcpy(mp->mntName, "/");
	get_block(dev, 2, buf);
	gp = (GD *)buf;
	bmap = mp->bmap = gp->bg_blocks_bitmap;
	imap = mp->imap = gp->bg_inodes_bitmap;
	iblock = mp->iblock = gp->bg_inode_table;
	printf("bmap=%d imap=%d iblock=%d\n", bmap, imap, iblock);

	// call iget( ) , which inc minode's refCount
	root = iget(dev, 2);	// get root inode
	mp->mntDirPtr = root;	// double link
	root->mntPtr = mp;
	// set proc CwDs
	for(i=0; i<NPROC; i++)			// set proc's CWD
		proc[i].cwd = iget(dev, 2); // each inc refcount by 1
	printf("mount : %smounted on / in", rootdev);
	return 0;
}

int main(int argc, char *argv[])
{
	char line[128], cmd[16], pathname[64];
	if(argc > 1)
		rootdev = argv[1];
	fs_init( );
	mount_root(rootdev);

	while(1){
		printf("P%d running: ", running->pid);
		printf("input command : ");
		fgets(line, 128, stdin);
		line[strlen(line)-1] = 0;
		if(line[0]==0)
			continue;
		sscanf(line, "%s %s", cmd, pathname);
		if(!strcmp(cmd, "ls"))
			ls(pathname);
		if(!strcmp(cmd, "cd"))
			chdir(pathname);
		if(!strcmp(cmd, "pwd"))
			pwd(running->cwd);
		if(!strcmp(cmd, "quit"))
			quit( );
	}
}

int quit()	// write a11 modified minodes to disk
{
	int i;
	for(i=0; i<NMINODES; i++){
		MINODE *mip = &minode[i];
		if(mip->refCount && mip->dirty){
			mip->refCount = 1;
			iput(mip);
		}
	}
	exit(0);
}

